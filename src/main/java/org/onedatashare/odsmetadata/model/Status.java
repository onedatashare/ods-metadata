package org.onedatashare.odsmetadata.model;

public enum Status {
    transferring, pending, failed, completed, started, starting
}
