package org.onedatashare.odsmetadata;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OdsMetadataApplication {

    public static void main(String[] args) {
        SpringApplication.run(OdsMetadataApplication.class, args);
    }

}
